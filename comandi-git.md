### Inizializzazione e Configurazione

**Inizializzazione del repository**
- `git init`: Inizializza un nuovo repository Git nella directory corrente.
    
**Visualizzazione dello stato del repository**
- `git status`: Mostra lo stato dei file nel repository (tracciati, non tracciati, modificati).
    
**Configurazione dell'utente**
- `git config user.name "nome_utente"`: Configura il nome utente solo per il repository corrente.
- `git config --global user.name "nome_utente"`: Configura il nome utente globalmente per tutti i repository dell'utente.
- 
**Configurazione dell'email**
- `git config user.email "nome_utente@example.com"`: Configura la email solo per il repository corrente.
- `git config --global user.email "nome_utente@example.com"`: Configura la email globalmente per tutti i repository dell'utente.


### Aggiunta e Commit di File

**Aggiungere file all'area di staging**
- `git add`: Aggiunge file o modifiche all'area di staging.
- `git add .`: Aggiunge tutti i file e le modifiche presenti nella directory corrente all'area di staging.
    
**Commit dei cambiamenti**
- `git commit -m 'messaggio'`: Registra le modifiche nell'area di staging nel repository con un messaggio di commit.
- `git commit --amend`: Modifica l'ultimo commit, permettendo di cambiare il messaggio o aggiungere modifiche.

**Rimuovere file**
- `rm nome_file`: Rimuove il file dalla directory di lavoro.
- `git add file eliminato`: Aggiunge la rimozione del file all'area di staging.
- `git commit -m 'rm file eliminato'`: Registra la rimozione del file nel repository con un messaggio di commit.


### Checkout

**Cambiamento di branch o revisione**
- `git checkout`: Passa a un branch diverso o a una revisione specifica.
- `git checkout -b`: Crea e passa su un nuovo branch


### Gestione del Log e dello Storico

**Visualizzazione del log**
- `git log`: Mostra la cronologia dei commit.
- `git log --oneline`: Mostra la cronologia dei commit in formato condensato (una linea per commit).
- `git log --oneline --all`: Mostra la cronologia dei commit di tutti i branch in formato condensato.
- `git show`: Mostra i dettagli di un commit specifico (default: l'ultimo commit).
    
**Reset dei commit**
- `git reset --hard`: Ripristina la directory di lavoro e l'indice al commit specificato, perdendo tutte le modifiche non commesse.
- `git revert`: Crea un nuovo commit che annulla le modifiche di un commit precedente.

**Differenze tra Versioni**
- `git diff #file1.txt #file2.txt` : Mostra le differenze tra due commit, o tra il codice attuale e l'ultimo commit.
- `git diff --stat #file1.txt #file2.txt`: Mostra una statistica dei cambiamenti (ad esempio, numero di linee aggiunte e rimosse).


### Stash (Memorizzazione Temporanea dei Cambiamenti)

**Creazione di uno stash**
- `git stash`: Salva le modifiche non committate temporaneamente per tornarci più tardi, cosi che non vengano creati dei commit tempooranei.

**Elenco degli stash**
- `git stash list`: Mostra una lista di tutti gli stash salvati.

Applicazione dello stash
- `git stash apply`: Riapplica lo stash seguito da un indice che lo identifica tra quelli presenti, se non si applica nessun indice si riapplica l'ultimo stash.

### Gestione di diversi branch

 **Unione dei Branch**
- `git merge branch_da_unire`: Unisce le modifiche di un altro branch nel branch corrente. (Assicurarsi di essere nel branch principale prima di fare il merge).

**Riallineare Branch**
- `git rebase master`: Allinea il branch corrente al master, riscrivendo la cronologia dei commit. Da utilizzare con cautela per evitare conflitti.


### Comandi per Interagire con i Repository Remoti

**Clonare un Repository**
- `git clone https://github.com/utente/progetto.git`: Crea una copia locale di un repository remoto.

**Inviare Modifiche**
- ``git push origin <branch>``: Invia le modifiche dal repository locale al repository remoto.

 **Recuperare Modifiche**
- `git fetch origin`: Recupera le modifiche dal repository remoto senza modificarne la copia locale.


### Comandi per la Gestione della Cronologia

**Annullare Commit**
- `git reset --hard commit_id`:  Annulla uno o più commit, ripristinando lo stato precedente ed elimina definitivamente i commit successivi a `commit_id`. Utilizza con cautela su repository remoti.

**Creare un Commit di Reversione**
- `git revert commit_id`: Crea un nuovo commit che annulla le modifiche di un commit precedente.

### Altri Comandi Utili

**Creare Tag**
- `git tag nome_tag commit_id` : Aggiunge un tag, esso crea un tag per un commit specifico, utile per marcare versioni di rilascio o altri punti importanti nella storia del progetto.

**Nascondere file**
- `.gitignore`: Si mettono le regole (file) che non vogliamo far vedere a git.